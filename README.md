# VR Target Practice 
**Hi there EtowTheOfficeCat here! Today i´ll present you the game I created for my midterm exams at the GPB.**

![](https://media.giphy.com/media/jUiko9b4A2rfNAB3eh/giphy.gif)

**Here Short Description of the Game:** 

VR Target Practice is a VR highscore survival mini game in which the player has to avoid getting hit by enemy targets by destroying them 
with a handheld Phaser Blaster. The Player also has to shoot down blue targets wandering around the map to collect points he will need to 
unlock new features. For Example unlocking new Platform to which the player can teleport to get closer to blue targets or escape the Enemy Targets,
or the Ability to Stop the Time for 10 seconds to shoot down targets and or prepare a new plan of defense, but also just to be able to reload his
phaser blaster. 

**GameBuild Download**

https://www.dropbox.com/s/v9h8juoz79omgyx/VRTargetPractice1.rar?dl=0

# Preview Video of the Game : 

https://youtu.be/cCn7jdJhnn4

[![Watch the video](https://media.giphy.com/media/ftpmH27rzrLRORpMRS/giphy.gif)](https://youtu.be/cCn7jdJhnn4)
# Pictures of the Game: 

![Alt Text](https://i.imgur.com/mY6tV9d.png)
Game main platform where the player starts.

![Alt Text](https://i.imgur.com/bDkgFXj.png)
Main menu with instructions

![Alt Text](https://i.imgur.com/uhLhHQc.png)
EnemyTargets(Red) and PointsTarget(blue) spawners. the targets will spawn rendomly within the colored area. BlueTarget will then move to another blue spawning point (chosen at random) while the red target will always go towards the player position. you can also see the 4 platforms that the player can teleport to (after he unlocked them)

This Game was created for my Game Design midterms exams at the GPB. Created with Unity 2019.1.6f1, Coded in C# 

All the assets are custom made, find them on my [Artstation](https://www.artstation.com/artwork/k4B4kl)
